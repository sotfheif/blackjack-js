export function shuffled(arr) { //tested a bit
    let indexArray = [...Array(arr.length).keys()]
    const res = []
    while (indexArray.length > 0) {
        const ind = Math.floor(Math.random() * indexArray.length)
        res.push(arr[indexArray[ind]])
        indexArray.splice(ind, 1)
        /*
        console.log("indexArray=" + indexArray)
        console.log("indexArray.length=" + indexArray.length)
        console.log("ind="+ind)
        console.log("res=" + res)
        */
    }
    return res
}

export const getMethods = (obj) => Object.getOwnPropertyNames(obj)
    .filter(item => typeof obj[item] === 'function')

export function containsMoreThanNItemsThat(arr, n, predicate) {
    console.log(`containsMoreThanNItemsThat(arr, n, predicate)`)
    console.log(`arr[0]`)
    let itemCount = 0
    for (const item of arr) {
        console.log(`itemCount=${itemCount}, item.isResultShown=${item.isResultShown}`)
        if (predicate(item)) { 
            itemCount++
            console.log(`itemCount=${itemCount}`)
            if (itemCount > n) { return true }
        }
    }
    return false
}

export function clamp(number, from, to) {
    if (isNaN(number) || isNaN(from) || isNaN(to)) {
        throw new Error('All parameters should be numbers!')
    }
    if (from > to) {
        throw new Error('"from" should be less than or equal to "to" (from <= to) ')
    }
    return Math.min(Math.max(from, number), to)
}