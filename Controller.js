import { Card } from "./Card.js"
import { suitFileName, rankFileName, CARD_DRAW_DURATION_MS, BlackjackTypes, PlayerType } from "./Constants.js"

//TODO add minimum(like 1 px) margin/padding left and bot for each deck card to previous one so that deck will be 3d


export class Controller {
  constructor() {

  }
  neighbourCardOffsetPx = 33
  cardWidthPx = 100
  dealerCardPlacePx = { left: 100, top: 20 }
  playerCardPlacePx = { left: 100, top: 200 }
  deckPlacePx = { left: 800, top: 110 }
  deckImgId = "deck-img"
  balanceTextId = "balance-text"
  gameoverMessId = "gameover-message"

  activeHandFirstCardClassName = "active-hand-first-card"
  activeHandCardExceptFirstClassName = "active-hand-card-except-first"
  otherHandCardExceptFirstClassName = "other-hand-card-except-first"

  getDealerCardPlacePx() {
    let elRect = document.getElementById("dealer-cards").getBoundingClientRect()
    let left = elRect.left
    let top = elRect.top
    console.log("getdealerplc left =" + left + ", top =" + top)

    return { left: left, top: top }
  }
  getPlayerCardPlacePx() {
    let elRect = document.getElementById("player-cards").getBoundingClientRect()
    let left = elRect.left
    let top = elRect.top
    console.log("getplayerplc left =" + left + ", top =" + top)
    return { left: left, top: top }
  }
  getDeckPlacePx() {
    let elRect = document.getElementById("deck-img").getBoundingClientRect()
    let left = elRect.left
    let top = elRect.top
    return { left: left, top: top }
  }

  build() {
    return new this()
  }


  move(source, left, top, fill = 'forwards') {
    console.log("source.style.left= " + parseInt(source.style.left) + ", source.style.top=" + parseInt(source.style.top))

    const x = left - parseInt(source.style.left)
    const y = top - parseInt(source.style.top)

    const keyFrames =
      [
        { transform: 'translate(0)' },
        { transform: `translate(${x}px, ${y}px)` }
      ]

    let anim = source.animate(keyFrames, {
      duration: CARD_DRAW_DURATION_MS,
      fill: fill,
      easing: "ease-in-out",
    })
    return anim.finished
  }

  //pass fill = 'none' to return to orig place after animation
  moveAndTurnOver(source, left, top, fill = 'forwards', card) {
    return this.move(source, left, top, fill).then(
      this.setCardImgSrc.bind(this, source, card))
  }

  moveAndTurnOverCards(sourceAr, leftAr, topAr, fillAr, cardAr) {
    let animAr = prepareMoveAndTurnOverCardsAnimAr(sourceAr, leftAr, topAr, fillAr, cardAr)
    this.chainAnimations(animAr)
  }

  prepareMoveAndTurnOverCardsAnimAr(sourceAr, leftAr, topAr, fillAr, cardAr) {
    let animAr = []
    let i = 0
    while (i < sourceAr.length) {
      const x = leftAr[i] - parseInt(sourceAr[i].style.left)
      const y = topAr[i] - parseInt(sourceAr[i].style.top)
      console.log(`x=${x}, y=${y}`)
      const keyFrames =
        [
          { transform: 'translate(0)' },
          { transform: `translate(${x}px, ${y}px)` }
        ]

      const anim = new Animation(
        new KeyframeEffect(sourceAr[i], keyFrames,
          {
            duration: CARD_DRAW_DURATION_MS,
            fill: fillAr[i],
            easing: "ease-in-out",
          }))
      anim.onfinish = this.setCardImgSrc.bind(this, sourceAr[i], cardAr[i])
      animAr.push(moveAnim)
      i++
    }
    console.log("animAr = " + animAr)
    return animAr
  }

  chainAnimations(animations) {
    const s = animations.reduce((accumulator, currentValue) => {
      return accumulator.then(() => {
        currentValue.play()
        return currentValue.finished
      })
    }, Promise.resolve()).then(
      () => console.log('chainanims done'),
      (e) => { console.log("e=" + e) }
    )
  }

  addCardBackOnDeck(id = "") {
    let deckPlace = this.getDeckPlacePx()
    console.log("addCardBackOnDeck left " + deckPlace.left + " top " + deckPlace.top)
    const cardBackImg = this.createCardBack()
    cardBackImg.style.left = deckPlace.left
    cardBackImg.style.top = deckPlace.top
    if (id != "") { cardBackImg.setAttribute("id", id) }
    document.body.append(cardBackImg)
    return document.getElementById(id)
  }

  addCardFrontImg(card, left, top) {
    let cardFrontImg = this.createCardFront(card)
  }

  createCardBack() {
    const cardBackImg = this.createCardImg()
    cardBackImg.src = "./res/card_images/svg/back.svg"
    return cardBackImg
  }

  createCardImg(isSmall=false, positionAbsolute=true/*true*/) {
    const img = document.createElement("img")
    if (positionAbsolute && !isSmall) {
      img.style.position = "absolute"
    }
    if(isSmall) {
      img.setAttribute("class", "smallcard removable")
    } else {
    img.setAttribute("class", "card removable")
    }
    return img
  }

  createCardFront(card, isSmall = false, isPositionAbsolute = true) {
    let cardFrontImg = this.createCardImg(isSmall, isPositionAbsolute)
    cardFrontImg.setAttribute("src", this.getCardImgPath(card))
    return cardFrontImg
  }

  getCardImgPath(card) {
    console.log("getCardImgPath " + card.suit + " " + card.rank)
    console.log("getCardImgPath " + suitFileName.get(card.suit) + " " + rankFileName.get(card.rank))

    return `./res/card_images/svg/${suitFileName.get(card.suit)}_${rankFileName.get(card.rank)}.svg`
  }



  setCardImgSrc(el, card) {
    console.log("setCardImgSrc " + card.suit + " " + card.rank)
    el.setAttribute("src", this.getCardImgPath(card))
  }

  playerHit(cards) {
    const i = cards.length - 1
    const prom = this.drawPlayerCard(cards[i], i, PlayerType.player)
      .then(() => this.updateHandWorth(PlayerType.player,
        Card.getCardsWorth(cards.slice(0, i + 1))))
    return prom
  }

  playerSplitHand(balance, activeHandNumber, activeHandCards, activeHandBet, 
    activeHandWorth, otherHandNumber, otherHandCards, otherHandBet,
     otherHandWorth, playerType=PlayerType.player
    ) {
    console.log(`split: activeHand={ {${activeHandCards[0].suit}, ${activeHandCards[0].rank}} }, newOtherHand={ {${otherHandCards[0].suit}, ${otherHandCards[0].rank}}`)
    this.updateBalance(balance)
    this.updateActiveAndOtherHand(activeHandNumber, activeHandCards, activeHandBet, 
      activeHandWorth, otherHandNumber, otherHandCards, otherHandBet,
      otherHandWorth, playerType)
  }

  updateActiveAndOtherHand(activeHandNumber, activeHandCards, activeHandBet, 
    activeHandWorth, otherHandNumber, otherHandCards, otherHandBet,
     otherHandWorth, playerType=PlayerType.player
    ) {
    this.updateActiveHand(activeHandNumber, activeHandCards, activeHandBet, activeHandWorth, playerType)
    this.updateOtherHand(otherHandNumber, activeHandNumber, otherHandCards, otherHandBet, otherHandWorth)
  }

  drawPlayerCard(card, prevCardCount, playerType, hidden = false, isSplitGame = false, splitGameCard=null, splitGameCardId = null) {
    console.log("drawPlayerCard " + card.suit + " " + card.rank)
    let cardImgId = `${playerType}-card${prevCardCount}`
    let playerCardPlace
    if (playerType === PlayerType.player) {
      playerCardPlace = this.getPlayerCardPlacePx()
    } else if (playerType === PlayerType.dealer) {
      playerCardPlace = this.getDealerCardPlacePx()
    }
    let leftC = playerCardPlace.left + prevCardCount *
      this.neighbourCardOffsetPx
    let topC = playerCardPlace.top
    let el = this.addCardBackOnDeck(cardImgId)
    if (hidden) {
      return this.move(el, leftC, topC, "forwards")
    } else {
      return this.moveAndTurnOver(el, leftC, topC, "forwards", card)
    }
  }

  updateActiveHand(activeHandNumber, cards, bet, worth, playerType=PlayerType.player){
    //update texts
    let numberBetEl = document.getElementById("bet-text")
    numberBetEl.textContent = `Hand ${activeHandNumber} Bet: ${bet}`
    let worthEl = document.getElementById("player-worth-text")
    worthEl.textContent = worth
    //update card images
    this.replaceActiveHandCards(cards, playerType)
  }
  
  replaceActiveHandCards(cards, playerType=PlayerType.player) {
    this.clearAllActiveHandTopLvlCards(playerType)
    this.addActiveHandCards(cards, playerType)
  }

  clearAllActiveHandTopLvlCards(playerType=PlayerType.player) {
    let i = 0
    let el = document.getElementById(`${playerType}-card${i}`)
    while(el!=null) {
      el.parentNode.removeChild(el);
      i++
      el = document.getElementById(`${playerType}-card${i}`)
    }
  }

  clearAllActiveHandCards(playerType=PlayerType.player) {
    if (playerType==PlayerType.player) {
      this.removeAllChildren(document.getElementById("player-cards"));
    } else if(playerType==PlayerType.dealer) {
      this.removeAllChildren(document.getElementById("dealer-cards"));
    }
  }

  addActiveHandCards(cards, playerType=PlayerType.player) {
    if (playerType==PlayerType.player) {
      this.addActiveHandCardsChildren(document.getElementById("player-cards"), cards, "player")
    } else if (playerType==PlayerType.dealer) {
      this.addActiveHandCardsChildren(document.getElementById("dealer-cards"), cards, "dealer")
    }
  }

  addActiveHandCardsChildren(element, cards, cardIdPrefix="player") {
    let i = 0
    for (const card of cards) {
      let cardImgId = `${cardIdPrefix}-card${i}`
      let cardImg = this.createCardFront(card, false, false)
      cardImg.setAttribute("id", cardImgId)
      if (i != 0) {
        this.addElemClass(cardImg, this.activeHandCardExceptFirstClassName)
      }
      element.append(cardImg)
      i++
    }
  }

  removeAllChildren(element) {
    while (element.firstChild) {
      element.removeChild(element.lastChild);
    }
  }

  updateOtherHand(otherHandNumber, activeHandNumber, cards, otherHandBet, otherHandWorth) {
    //compute element number
    let otherHandElNumber = otherHandNumber
    if (activeHandNumber < otherHandNumber) {
      otherHandElNumber--
    }
    //replace texts
    console.log(document.getElementsByClassName("other-hand"))
    const otherHandEl = document.getElementsByClassName("other-hand")[otherHandElNumber]
    const handNumberBet = otherHandEl.getElementsByClassName("other-hand-number-bet")[0]
    handNumberBet.textContent = `Hand ${otherHandNumber} Bet: ${otherHandBet}`
    const otherHandWorthEl = otherHandEl.getElementsByClassName("other-hand-worth")[0]
    otherHandWorthEl.textContent = otherHandWorth
    //replace card images
    const otherHandCardsEl = otherHandEl.getElementsByClassName("other-hand-cards")[0]
    this.replaceOtherHandCards(cards, otherHandCardsEl)
  }

  replaceOtherHandCards(cards, otherHandEl) {
    this.clearAllOtherHandCards(otherHandEl)
    this.addOtherHandCards(cards, otherHandEl)
  }

  clearAllOtherHandCards(otherHandEl) {
    this.removeAllChildren(otherHandEl);
  }

  addOtherHandCards(cards, otherHandEl) {
    let i = 0
    for (const card of cards) {
      let cardImg = this.createCardFront(card, true)
      if (i != 0) {
        this.addElemClass(cardImg, this.otherHandCardExceptFirstClassName)
      }
      otherHandEl.append(cardImg)
      i++
    }
  }

  t2() { console.log("animend") }

  drawDealerCard(card) {

  }

  cleanUpUiOnGameStart() {
    console.log("cleanUpGameUi()")
    document.getElementById("gameover-message").textContent = ""
    document.getElementById("player-worth-text").textContent = ""
    document.getElementById("dealer-worth-text").textContent = ""
    let otherHandsWorths = document.getElementsByClassName("other-hand-worth")
    for (const worthEl of otherHandsWorths) {
      worthEl.textContent = ""
    }
    let otherHandsNumbersBets = document.getElementsByClassName("other-hand-number-bet")
    for (const numberBetEl of otherHandsNumbersBets) {
      numberBetEl.textContent = ""
    }
  }

  revealElementsOnGameStart() {
    this.revealElementsByClass("game")
  }

  revealOptionalElementsOnGameStart(balance, bet, playerHand, blackjackState) {
    //if (blackjackState != BlackjackTypes.nobody) { return } //TODO(mb check if commenting this out is ok)
    if (balance >= bet) {
      this.updateDoubleDownBtn(true)
      if ((playerHand.getSize() === 2) && (playerHand.cards[0].rank === playerHand.cards[1].rank)) {
        this.updateSplitBtn(true)
      }
    }
  }

  disableAllButtons() { // mb use during animations
    document.getElementsByClassName("btn").style.disabled = true
  }

  enableAllButtons() {
    document.getElementsByClassName("btn").style.disabled = false
  }

  updatePlayerButtons(hitBtnEnabled, standBtnEnabled, doubleDownBtnEnabled, splitBtnEnabled) {
    this.updateHitBtn(hitBtnEnabled)
    this.updateStandBtn(standBtnEnabled)
    this.updateDoubleDownBtn(doubleDownBtnEnabled)
    this.updateSplitBtn(splitBtnEnabled)
  }

  updateHitBtn(btnEnabled){
    let btn = document.getElementById("hit-btn")
    if(btnEnabled) {
      this.showElement(btn)
    } else { this.hideElement(btn)}
  }

  updateStandBtn(btnEnabled){
    let btn = document.getElementById("stand-btn")
    if(btnEnabled) {
      this.showElement(btn)
    } else { this.hideElement(btn) }
  }

  updateDoubleDownBtn(btnEnabled){
    console.log(`updateDoubleDownBtn(${btnEnabled})`)
    let btn = document.getElementById("double-down-btn")
    if(btnEnabled) {
      this.showElement(btn)
    } else { this.hideElement(btn) }
  }

  updateSplitBtn(btnEnabled){
    let btn = document.getElementById("split-btn")
    if(btnEnabled) {
      this.showElement(btn)
    } else { this.hideElement(btn) }
  }

  cleanUpUiOnShowResultsStage() {
    this.hideElementsByClass("game")
    this.hideElementsByClass("optional")
    //document.getElementById("bet-text").textContent = ""
  }

  showBet(bet, handId=0) {
    const betEl = document.getElementById("bet-text")
    betEl.textContent = "Hand " + handId + " Bet: " + bet
    betEl.style.visibility = "visible"
  }

  updateShowUiText(elemId, text) {
    const elem = document.getElementById(elemId)
    elem.textContent = text
    elem.style.visibility = "visible"
  }

  updateBalance(balance) {
    const text = "Balance: " + balance
    this.updateShowUiText(this.balanceTextId, text)
  }

  startGame(balance, bet, playerHand, dealerHand, /* blackjackState, newBalance */) {
    console.log("playerhand = " + playerHand.cards[0].rank + " " + playerHand.cards[0].suit + ', ' + playerHand.cards[1].rank + ' ' + playerHand.cards[1].suit)
    console.log("dealerhand = " + dealerHand.cards[0].rank + " " + dealerHand.cards[0].suit + ', ' + dealerHand.cards[1].rank + ' ' + dealerHand.cards[1].suit)
    this.hideBetDialog()
    this.hidePlayShowNextResultBtnDiv()
    this.revealElementsByClass("game")
    this.updateBalance(balance)
    this.showBet(bet)
    document.getElementById(this.deckImgId).style.visibility = "visible" //deck image
    return this.givePlayerStartHand(playerHand).then(() => { return this.giveDealerStartHand(dealerHand) }).then(() =>
      this.showHandWorth(playerHand.getWorth(), dealerHand.getWorth(false))
    )//.then(() => this.handleBlackJack(blackjackState, newBalance, dealerHand.cards[0], dealerHand.getWorth()))
  }

  showHandWorth(playerHandWorth, dealerHandWorth) {
    let playerWorthEl = document.getElementById("player-worth-text")
    let dealerWorthEl = document.getElementById("dealer-worth-text")
    playerWorthEl.textContent = playerHandWorth
    dealerWorthEl.textContent = dealerHandWorth
  }

  hideBetDialog() {
    this.hideElementsByClass("place-bet")
  }

  showBetDialog() {
    this.revealElementsByClass("place-bet")
  }

  hidePlayShowNextResultBtnDiv() {
    this.hideElementsByClass("play-show-next-result-btn-div")
  }

  showPlayShowNextResultBtnDiv() {
    this.revealElementsByClass("play-show-next-result-btn-div")
  }

  givePlayerStartHand(playerHand) {
    return this.drawPlayerCard(playerHand.cards[0], 0, PlayerType.player)
      .then(() => this.drawPlayerCard(playerHand.cards[1], 1, PlayerType.player))
  }

  giveDealerStartHand(dealerHand) {
    return this.drawPlayerCard(dealerHand.cards[0], 0, PlayerType.dealer, true)
      .then(() => this.drawPlayerCard(dealerHand.cards[1], 1, PlayerType.dealer))
  }

  dealerMove(dealerHand) {
    const cards = dealerHand.cards
    let i = 1

    const s = cards.slice(2).reduce((accumulator, card) => {
      return accumulator.then(() => {
        i++
        console.log(`updatehandworth() cards.slice(0,${i}):${cards.slice(0, i + 1)
          .map(card => card.rank)}`)
        const prom = this.drawPlayerCard(card, i, PlayerType.dealer)
          .then(() => this.updateHandWorth(PlayerType.dealer,
            Card.getCardsWorth(cards.slice(0, i + 1))))


        return prom
      })
    }, Promise.resolve())
    return s
  }

  toInsuranceStage() {
    this.updatePlayerButtons()
    document.getElementById("insurance-dialog").style.display = "flex"
    //TODO mb inform about constraints of insurance bet, e.g.: "(0-50)"
  }

  
  endInsuranceStage() {
    hideInsuranceDialog()
  }
  
  openInsuranceDialog() {
    document.getElementById("insurance-dialog").style.display = "flex"
  }

  closeInsuranceDialog() {
    document.getElementById("insurance-dialog").style.display = "none"
  }

  /*
  handleBlackJack(blackjackState, newBalance, dealerHiddenCard, dealerHandWorth) {
    if (blackjackState === BlackjackTypes.nobody) { return }
    this.showDealerHiddenCard(dealerHiddenCard, dealerHandWorth)

    let mess
    switch (blackjackState) {
      case BlackjackTypes.player:
        mess = "Blackjack! You win"
        break
      case BlackjackTypes.dealer:
        mess = "Dealer blackjack! You lose"
        break
      case BlackjackTypes.both:
        mess = "Double blackjack! Draw"
        break
      default:
    }
    this.cleanUpUiOnShowResultsStage()
    this.showHandResult(mess, newBalance)
    this.showPlayShowNextResultBtnDiv()
    //game.isOver = true
  }
  */

  showDealerHiddenCard(card, handWorth) {
    document.getElementById("dealer-card0").src = this.getCardImgPath(card)
    this.updateHandWorth(PlayerType.dealer, handWorth)
  }

  showHandResult(mess, newBalance, isLastHand = true) {
    this.updateBalance(newBalance)
    document.getElementById("gameover-message")
      .textContent = mess
    if (isLastHand) this.showBetDialog()
  }


  changePlayBtn(showNextHandRes) {
    console.log(`changePlayBtn showNextHandRes=${showNextHandRes}`)
    const playBtn = document.getElementById("play-btn")
    const showNextResultBtn = document.getElementById("show-next-result-btn")
    if (showNextHandRes) {
      showNextResultBtn.style.display = "inline"
      playBtn.style.display = "none"
    } else {
      playBtn.style.display = "inline"
      showNextResultBtn.style.display = "none"
    }
  }

  endGame() {
    //this.hideBetDialog()
    this.cleanUpUiOnShowResultsStage()
  }

  updateHandWorth(playerType, handWorth) {
    let id = `${playerType}-worth-text`
    document.getElementById(id).textContent = handWorth
  }

  removeElementsByClass(className) {
    const elements = document.getElementsByClassName(className);
    while (elements.length > 0) {
      elements[0].parentNode.removeChild(elements[0]);
    }
  }

  hideElement(element){
    element.style.visibility = "hidden"
  }

  showElement(element) {
    element.style.visibility = "visible"
  }

  hideElementsByClass(className) {
    const elements = document.getElementsByClassName(className);
    let i = -1
    while (++i < elements.length) {
      elements[i].style.visibility = "hidden";
    }
  }

  revealElementsByClass(className) {
    const elements = document.getElementsByClassName(className);
    let i = -1
    while (++i < elements.length) {
      elements[i].style.visibility = "visible";
    }
  }

  addElemClass(elem, additionalClass) {
    elem.setAttribute("class", elem.className + " " + additionalClass)
    return elem
  }
}