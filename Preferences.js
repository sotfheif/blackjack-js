export class Preferences {
    constructor(insuranceEnabled) {
        this.insuranceEnabled = insuranceEnabled
    }

    static build(insuranceEnabled = false) {
        return new this(insuranceEnabled)
    }

    setInsuranceEnabled(enabled = true) {
        this.insuranceEnabled = enabled
    }
}