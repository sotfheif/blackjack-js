import { BlackjackTypes, GameResult } from "./Constants.js"
import { Preferences } from "./Preferences.js"

export class Global {
    constructor(money, insuranceEnabled) {
        this.money = money
        this.preferences = Preferences.build(insuranceEnabled)
    }
    static build(money = 1000, insuranceEnabled = false) {
        return new this(money, insuranceEnabled)
    }
    placeBet(bet) {
        this.money -= bet
    }
    winSum(amount) {
        this.money = this.money + amount
    }
    handleBlackJack(blackjackState, bet, usedInsurance, insuranceBet) {
        switch (blackjackState) {
            case BlackjackTypes.player:
                this.winSum(Math.round(bet * 2.5))
                break
            case BlackjackTypes.both:
                this.winSum(bet)
                if(usedInsurance === true) {
                    this.winSum(3*insuranceBet)
                }
                break
            case BlackjackTypes.dealer:
                if(usedInsurance === true) {
                    this.winSum(3*insuranceBet)
                }
                break
            default:
        }
        return this.money
    }

    computeHandResult(gameResult, bet) {
        switch (gameResult) {
            case GameResult.draw:
                this.winSum(bet)
                break
            case GameResult.win:
                this.winSum(bet * 2)
                break
            case GameResult.loss:
                break
            default:
        }
        return this.money
    }
}