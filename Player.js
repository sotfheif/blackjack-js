import { Hand } from "./Hand.js"
import { containsMoreThanNItemsThat } from "./Utils.js"

export class Player {
    constructor(isDealer, hands, activeHandId=0) {
        this.isDealer = isDealer
        this.hands = hands
        this.activeHandId = activeHandId
    }
    static build(isDealer = false, bet=undefined) {
        let hands = []
        let hand = Hand.build([], 0, bet=bet)
        hand.isPlayed = true
        hands.push(hand)
        return new this(isDealer, hands)
    }
    
    split() {
        let newHand = this.getCurrentHand().split(this.getNumberOfHands())
        this.addHand(newHand)
        return newHand
    }

    tryGetNotPlayedHand() {
        let nextHand = this.hands.find((hand) => hand.isPlayed === false)
        console.log(`tryChangeActiveHand: nextHand.handNumber=${nextHand.handNumber}, this.activeHandId=${this.activeHandId}, this.getCurrentHand().getSize()=${this.getCurrentHand().getSize()}`)
        return nextHand
    }

    tryChangeActiveHand() {
        this.getCurrentHand().isPlayed = true
        let nextHand = this.hands.find((hand) => hand.isPlayed === false)
        if (nextHand === undefined) {
        } else {
          this.activeHandId = nextHand.handNumber
          console.log(`tryChangeActiveHand: nextHand.handNumber=${nextHand.handNumber}, this.activeHandId=${this.activeHandId}, this.getCurrentHand().getSize()=${this.getCurrentHand().getSize()}`)
        }
        return nextHand
    }

    tryChangeShowResultHand() {
        //this.getCurrentHand().isResultShown = true // better do this separately
        let nextHand = this.hands.find((hand) => hand.isResultShown === false)
        if (nextHand === undefined) {
        } else {
          this.activeHandId = nextHand.handNumber
        }
        return nextHand
    }

    hasHandNotPlayed() {
        return (this.hands.find((hand) => hand.isPlayed === false) != undefined)
    }

    hasMoreThan1HandNotPlayed() {
        return containsMoreThanNItemsThat(this.hands, 1, (hand) => hand.isPlayed === false)
    }

    hasHandWithResultNotShown() {
        return (this.hands.find((hand) => hand.isResultShown === false) != undefined)
    }

    hasMoreThan1HandWithResultNotShown() {
        return containsMoreThanNItemsThat(this.hands, 1, (hand) => hand.isResultShown === false)
    }

    addHand(hand) {
        this.hands.push(hand)   
    }
    getCurrentHand(){
        return this.hands[this.activeHandId]
    }

    drawCard(deck) {
        console.log("typeof deck = " + typeof deck)
        console.log("deck card count = " + deck.cards.length)
        return this.getCurrentHand().addCard(deck.giveCard())
    }
    
    drawFirstCards(deck) {
        this.drawCard(deck)
        this.drawCard(deck)
    }

    drawFirstCardsForSplitTest(deck) {
        this.getCurrentHand().addCard(deck.giveCard1ForSplitTest())
        this.getCurrentHand().addCard(deck.giveCard2ForSplitTest())
    }

    hasBlackjack() { //TODO check that this is not called in split hands (if it shouldn't, check rules)
        if (this.getCurrentHand().getSize() === 2 && this.getCurrentHand().getWorth() === 21) {
            return true
        } else { return false }
    }

    getHandWorth() {
        return this.getCurrentHand().getWorth()
    }

    getNumberOfHands() {
        return this.hands.length
    }
}