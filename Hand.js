import { Card } from "./Card.js"

export class Hand {
    constructor(cards, handNumber, bet/*, isPlayed = false, isResultShown = false*/) {
        this.cards = cards
        this.bet = bet
        this.handNumber = handNumber
        this.isPlayed = false // isPlayed
        this.isResultShown = false // isResultShown
        this.insuranceBet = 0
        this.usedInsurance = false
    }
    handNumber
    cards
    bet
    static build(cards, handNumber, bet = 0) {
        return new this(cards, handNumber, bet)
    }

    increaseBet(amount) {
        this.bet += amount
    }

    insuranceBet(amount) {
        this.usedInsurance = true
        this.insuranceBet = amount
    }

    clearInsuranceBet() {
        this.usedInsurance = false
        this.insuranceBet = 0
    }
    
    /* mb use worth property and calcWorth() method which updates
    property value instead
    */
    getWorth(include1stCard = true) {
        return include1stCard ? Card.getCardsWorth(this.cards)
            : Card.getCardsWorth(this.cards.slice(1))
    }

    getSize() {
        return this.cards.length
    }

    addCard(card) {
        this.cards.push(card)
        return this.cards
    }
    
    split(handNumber) {
        if (this.getSize() != 2) { console.log("error: hand size>2"); return } //check just in case //TODO remove or move this check
        let newHandCard = this.cards.pop()
        return Hand.build([newHandCard], handNumber, this.bet)
    }
}