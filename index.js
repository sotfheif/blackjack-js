import { Game } from './Game.js'
import { Global } from './Global.js'
import { rules } from './Rules.js'
import { Controller } from './Controller.js'
import { BlackjackTypes, PlayerType } from './Constants.js'
import { clamp } from './Utils.js'

//TODO check error on blackjack
//TODO test insurance

/* These TODOs look obsolete 
//TODO make split button work, including case with 2 aces (see rules about being able to draw only 1 card)
//TODO update split btn state after drawing second card in split hand
*/

console.log("index.js start")

/*
var millisecondsToWait = 5000;
setTimeout(function() {
}, millisecondsToWait);
*/

const global = Global.build()
const controller = new Controller()
let betInput = document.getElementById("bet-input")

let game = Game.build(rules)

let playBtn = document.getElementById("play-btn")
playBtn.addEventListener("click", playBtnClick)

let showNextResultBtn = document.getElementById("show-next-result-btn")
showNextResultBtn.addEventListener("click", showNextResultBtnClick)

let hitBtn = document.getElementById("hit-btn")
hitBtn.addEventListener("click", playerHit)

//let okBtn = document.getElementById("gameover-ok-btn")

let standBtn = document.getElementById("stand-btn")
standBtn.addEventListener("click", playerStand)


let doubleDownBtn = document.getElementById("double-down-btn")
doubleDownBtn.addEventListener("click", playerDoubleDown)

let splitBtn = document.getElementById("split-btn")
splitBtn.addEventListener("click", playerSplit)

let insuranceBetInput = document.getElementById("insurance-bet-input")

let insuranceBetBtn = document.getElementById("insurance-bet-btn")
insuranceBetBtn.addEventListener("click", playerInsuranceBet)

let insuranceSkipBtn = document.getElementById("insurance-skip-btn")
insuranceSkipBtn.addEventListener("click", playerInsuranceSkip)


function startGame() {
  controller.cleanUpUiOnGameStart()
  controller.revealElementsOnGameStart()
  console.log("clas  len" + document.getElementsByClassName("removable").length)
  controller.removeElementsByClass("removable")
  let betText = betInput.value
  let bet = parseInt(betText)
  if (isNaN(bet)) {bet = 0}
  console.log('betText=' + betText + ' bet=' + bet)
  global.placeBet(bet)
  game.start(bet)
  console.log("game started")
  console.log(game.dealer.getCurrentHand().getSize() + " " +
    game.dealer.getCurrentHand().getWorth() + " " + game.player.getCurrentHand().getSize() +
    " " + game.player.getCurrentHand().getWorth())
  let balance = global.money
  const blackjackState = game.checkBlackjack()
  game.setBlackjackState(blackjackState)

  controller.revealOptionalElementsOnGameStart(balance, bet, game.player.getCurrentHand(), blackjackState)
  controller.startGame(balance, bet, game.player.getCurrentHand(), game.dealer.getCurrentHand())
  .then(() => { 
    if (global.preferences.insuranceEnabled && game.dealer.getCurrentHand().cards[1].rank === 11) {
      toInsuranceStage()
    } else {
      handleBlackjackState(blackjackState)
    }
   })

}

function toInsuranceStage() {

  controller.toInsuranceStage()
}

function playerInsuranceBet() {
  let insuranceBetText = insuranceBetInput.value
  let parsedInsuranceBet = parseInt(insuranceBetText)
  if (isNaN(parsedInsuranceBet)) {parsedInsuranceBet = 0}
  let mainBet = game.player.getCurrentHand().bet
  let clampedInsuranceBet = clamp(number = parsedInsuranceBet, from = 0, to = Math.floor(mainBet/2))
  global.placeBet(clampedInsuranceBet)
  game.playerInsuranceBet(clampedInsuranceBet)
  endInsuranceStage(global.money)
}

function playerInsuranceSkip() {
  endInsuranceStage(global.money)
}

function endInsuranceStage(newBalance) {
  controller.updateBalance(newBalance)
  controller.endInsuranceStage()
  handleBlackjackState(game.blackjackState)
}


function playerHit(shouldUpdateSplitBtn = true) {
  console.log("playerHit()")
  let cards = drawPlayerCard()
  
  let currentHand = game.player.getCurrentHand()
  //let nextHand = game.player.tryGetNotPlayedHand()
  return controller.playerHit(cards).then(() => {
    if (game.player.getHandWorth() > 21) {
      playerStand()
      /*
      let nextHand = game.player.tryChangeActiveHand()
      if (nextHand === undefined) {
        tryShowNextHandResult()
      } else {
        //game.player.tryChangeActiveHand()
      }
      */
      return Promise.reject("overdraft")
    } else {
      controller.updateSplitBtn(shouldUpdateSplitBtn && (currentHand.getSize() === 2) && (global.money >= currentHand.bet) && (currentHand.cards[0].rank === currentHand.cards[1].rank))
    }
  })
}

function updatePlayerButtons(balance, activeHand) {
  let hitBtnEnabled = !activeHand.isPlayed
  let standBtnEnabled = !activeHand.isPlayed
  let doubleDownBtnEnabled = (balance >= activeHand.bet)
  let splitBtnEnabled = ((activeHand.getSize() === 2) &&
   (activeHand.cards[0].rank === activeHand.cards[1].rank))
  controller.updatePlayerButtons(hitBtnEnabled, standBtnEnabled, doubleDownBtnEnabled, splitBtnEnabled)
}

function playerStand() {
  console.log("playerStand()")
  let activeHand = game.player.getCurrentHand()
  let nextHand = game.player.tryChangeActiveHand()
  if (nextHand === undefined) {
    console.log("nextHand === undefined")
    dealerMove(game.dealer).then(() => tryShowNextHandResult())
  } else {
    console.log("nextHand != undefined")
    controller.updateActiveAndOtherHand(nextHand.handNumber, 
      nextHand.cards, nextHand.bet, nextHand.getWorth(),
      activeHand.handNumber, activeHand.cards, activeHand.bet,
      activeHand.getWorth(), PlayerType.player)
    controller.updateDoubleDownBtn(global.money>=activeHand.bet)
  }
}

function playerDoubleDown() {// TODO mb should be available only when enough balance
  controller.hideElement(doubleDownBtn)
  let oldBet = game.player.getCurrentHand().bet
  if (oldBet > global.money) { return }
  global.placeBet(oldBet)
  game.player.getCurrentHand().increaseBet(oldBet)
  controller.showBet(game.player.getCurrentHand().bet)
  controller.updateBalance(global.money)
  return playerHit(false).then(playerStand, () => {console.log("onrejected")})
}

function playerSplit() {
  if (game.player.getNumberOfHands() >= 4) { 
    console.log("you can split up to 3 times"); 
    return 
  }
  if (game.wasSplit === false) { game.wasSplit = true }
  let currentBet = game.player.getCurrentHand().bet
  if (currentBet > global.money) { return }
  controller.hideElement(splitBtn)
  let activeHand = game.player.getCurrentHand()
  let newOtherHand = game.player.split()
  global.placeBet(currentBet)
  controller.updateDoubleDownBtn(global.money >= currentBet)
  controller.playerSplitHand(global.money, activeHand.handNumber, 
    activeHand.cards, activeHand.bet, activeHand.getWorth(),
    newOtherHand.handNumber, newOtherHand.cards, newOtherHand.bet,
    newOtherHand.getWorth(), PlayerType.player
    )
}

function drawPlayerCard() {
  return game.drawPlayerCard()
}

function dealerMove(dealer) {
  controller.showDealerHiddenCard(dealer.getCurrentHand().cards[0], dealer.getHandWorth())
  while (dealer.getHandWorth() < 17) {
    console.log(`dealer worth ${dealer.getHandWorth()}, need to draw more`)
    dealer.drawCard(game.deck)
  }
  console.log(`dealer worth ${dealer.getHandWorth()}, is enough`)
  return controller.dealerMove(dealer.getCurrentHand())
}

function isGameOver() {

}

/*
document.getElementById("test-btn").addEventListener("click", test)
function test() {}
*/

function handleBlackjackState(blackjackState) {
  if (blackjackState != BlackjackTypes.nobody) {
    controller.showDealerHiddenCard(game.dealer.getCurrentHand().cards[0], game.dealer.getHandWorth());
    const mess = game.handleBlackJack(blackjackState);
    controller.cleanUpUiOnShowResultsStage()
    let newBalance = global.handleBlackJack(blackjackState, bet, game.player.getCurrentHand().usedInsurance, game.player.getCurrentHand().insuranceBet)
    controller.showHandResult(mess, newBalance)
    controller.showPlayShowNextResultBtnDiv()
  }
}

function drawGameUi() {
  controller.drawGameUi()
}



function tryShowNextHandResult() {
  if(!game.isShowResultsStage) { 
    controller.cleanUpUiOnShowResultsStage()
    game.toShowResultsStage()
  }
  console.log(`game.player.hands.length=${game.player.hands.length}`)
  const hasMoreThan1HandResultNotShown = game.player.hasMoreThan1HandWithResultNotShown()

  controller.changePlayBtn(hasMoreThan1HandResultNotShown)

  if (game.wasSplit === true) {
  let oldHand = game.player.getCurrentHand()
  let newHand = game.player.tryChangeShowResultHand()
  
  controller.updateActiveAndOtherHand(newHand.handNumber, newHand.cards, newHand.bet, 
    newHand.getWorth(), oldHand.handNumber, oldHand.cards, oldHand.bet,
    oldHand.getWorth())
  }
  /*
  if (game.player.hasHandWithResultNotShown() || hand === undefined ) {
    game.isOver = true
    controller.endGame()// TODO change this
    startGame()
  } else {
  */
    const res = game.getResult()
    const newBalance = global.computeHandResult(res.result, game.player.getCurrentHand().bet)
    controller.showHandResult(res.mess, newBalance, !hasMoreThan1HandResultNotShown)
    game.player.getCurrentHand().isResultShown = true
    controller.showPlayShowNextResultBtnDiv()

    if (game.player.hasHandWithResultNotShown()) {
      game.isOver = true
    }
  //}
}

function playBtnClick() {
  startGame()
}

function showNextResultBtnClick() {
  if (game.isOver) {
  }
  tryShowNextHandResult()
}
